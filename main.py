def read_word_search(file_path):
    # Open the file in read mode using a context manager
    with open(file_path, 'r') as file:
        # Read the first line to get the dimensions of the grid
        dimensions = file.readline().strip().split('x')
        # Convert the dimensions to integers
        rows, cols = int(dimensions[0]), int(dimensions[1])

        # Read the grid of characters
        # Use list comprehension to read each row, split it into characters, and strip whitespace
        grid = [file.readline().strip().split() for _ in range(rows)]

        # Read the list of words to find
        # Use list comprehension to read each line, strip whitespace, and form a list
        words_to_find = [word.strip() for word in file.readlines()]

    # Return the dimensions, grid, and list of words
    return rows, cols, grid, words_to_find

def find_word(grid, word):
    # Iterate over each row in the grid
    for row in range(len(grid)):
        # Iterate over each column in the row
        for col in range(len(grid[0])):
            # Check all eight possible directions
            for delta_row, delta_col in [(-1, 0), (1, 0), (0, -1), (0, 1), (-1, -1), (-1, 1), (1, -1), (1, 1)]:
                found = True
                # Check each character in the word
                for i in range(len(word)):
                    # Calculate the position in the grid based on the direction
                    current_row, current_col = row + i * delta_row, col + i * delta_col
                    # Check if the position is within the grid bounds and if the characters match
                    if 0 <= current_row < len(grid) and 0 <= current_col < len(grid[0]) and grid[current_row][current_col] != word[i]:
                        found = False
                        break
                # If the word is found, return the result
                if found:
                    return f"{word} {row}:{col} {current_row}:{current_col}"

    # If the word is not found in any direction, return None
    return None

def main(file_path):
    # Call the read_word_search function to get the necessary data from the input file
    rows, cols, grid, words_to_find = read_word_search(file_path)

    # Find each word in the grid
    for word in words_to_find:
        result = find_word(grid, word)
        # Print the result for each word
        print(result)

if __name__ == "__main__":
    # Specify the path to the input file
    file_name = "example_input_file1.txt"

    # Call the main function with the file path
    main(file_name)

